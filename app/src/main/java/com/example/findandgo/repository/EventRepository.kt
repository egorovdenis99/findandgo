package com.example.findandgo.repository

import com.example.findandgo.network.EventsService
import com.example.findandgo.ui.FilterState
import retrofit2.Call
import vds.example.myapplication.models.EventResponse

class EventRepository(private val apiService: EventsService){
    fun loadEvents(filter: FilterState): Call<EventResponse>{
        return apiService.loadEvents(
            hashMapOf(
                "expand" to "place,location,date",
                "fields" to "id,place,title,description,images,bodyText,location,dates,is_free",
                "text" to "text",
                "location" to "spb",
                "is_free" to "${filter.isFree}"
            )
        )
    }

    fun loadLocationInfo(placeId: Int) = apiService.loadLocationInfo(placeId)
}