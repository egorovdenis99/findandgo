package com.example.findandgo.ui

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_map.*


class EventsMapFragment: Fragment(){

    private var googleMap: GoogleMap? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        retainInstance = true
        val map = SupportMapFragment.newInstance()
        map.getMapAsync {
            it.mapType = GoogleMap.MAP_TYPE_NORMAL

            it.clear()
            googleMap = it
            googleMap?.setOnMarkerDragListener(object: GoogleMap.OnMarkerDragListener{
                override fun onMarkerDragEnd(p0: Marker?) {
                    floatingActionButton.show()
                }

                override fun onMarkerDragStart(p0: Marker?) {
                    floatingActionButton.hide()
                }

                override fun onMarkerDrag(p0: Marker?) {
                }

            })
            updateMyLocation()
        }
        activity?.supportFragmentManager!!.beginTransaction()
            .replace(com.example.findandgo.R.id.mapFrame, map)
            .commit()

        floatingActionButton.setOnClickListener { updateMyLocation() }

    }

    private fun updateMyLocation(){
        if (ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION) == 0){
            val fusedLocationManager = LocationServices.getFusedLocationProviderClient(activity!!)
            val locationCallback = object : LocationCallback(){
                override fun onLocationResult(locationResult: LocationResult?) {
                    super.onLocationResult(locationResult)
                    val googlePlex = CameraPosition.builder()
                        .target(LatLng(locationResult!!.lastLocation.latitude, locationResult.lastLocation.longitude))
                        .zoom(10.0f)
                        .bearing(0.0f)
                        .tilt(45.0f)
                        .build()
                    googleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 1000, null)

                    val markerOptions = MarkerOptions().apply {
                        position(LatLng(locationResult.lastLocation.latitude, locationResult.lastLocation.longitude))
                    }

                    googleMap?.addMarker(markerOptions)

                }
            }
            fusedLocationManager.requestLocationUpdates(LocationRequest(), locationCallback, null)
        }
        else requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION), 1)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 1){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                updateMyLocation()
            }

        }
    }

    override fun onCreateView(p0: LayoutInflater, p1: ViewGroup?, p2: Bundle?): View? {
        return p0.inflate(com.example.findandgo.R.layout.fragment_map, p1, false)
    }
}