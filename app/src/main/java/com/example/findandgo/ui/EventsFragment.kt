package com.example.findandgo.ui

import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.findandgo.DependencyModule
import com.example.findandgo.R
import com.example.findandgo.adapter.EventAdapter
import com.example.findandgo.adapter.ItemOffsetDecorator
import com.example.findandgo.presenter.EventState
import com.example.findandgo.presenter.EventsPresenter
import com.example.findandgo.view.MvpView
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.events_fragment.*
import vds.example.myapplication.models.Event

class EventsFragment: Fragment(), MvpView {

    private val eventAdapter = EventAdapter()
    private lateinit var presenter: EventsPresenter
    var messageListener: MessageListener<Event>? = null
    private lateinit var fusedLocationManager: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        initLocationManager()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.events_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        retainInstance = true

        presenter = DependencyModule.providePresenter().apply {
            bindView(this@EventsFragment)
            restoreState()
        }

        eventsRecycler.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = eventAdapter.apply {
                onEventClickListener = object : EventAdapter.OnEventClickListener{
                    override fun onEventClick(event: Event) {
                           messageListener?.onMessage(event)
                    }
                }
            }
            addItemDecoration(ItemOffsetDecorator(10, 10, 10, 10))
        }

        findButton.setOnClickListener {
            val fragment = FiltersBottomSheetFragment().apply {
                messageListener = object : MessageListener<FilterState>{
                    override fun onMessage(filter: FilterState) {
                        presenter.onFiltersSelected(filter)
                    }

                }
            }
            fragment.show(activity?.supportFragmentManager, "")
        }
    }

    override fun render(state: EventState) {
        if (state.isLoading)
            progressBar.visibility = View.VISIBLE
        else progressBar.visibility = View.GONE

        if (state.isError)
            errorTextView.visibility = View.VISIBLE
        else errorTextView.visibility = View.GONE

        if (state.events != null){
            eventAdapter.updateEvents(state.events!!)
            eventsRecycler.visibility = View.VISIBLE
        }
        else eventsRecycler.visibility = View.GONE

        findButton.visibility = View.GONE
    }


    fun initLocationManager(){
        if (ContextCompat.checkSelfPermission(activity!!, android.Manifest.permission.ACCESS_FINE_LOCATION) == 0){
            fusedLocationManager = LocationServices.getFusedLocationProviderClient(activity!!)
            locationCallback = object : LocationCallback(){
                override fun onLocationResult(p0: LocationResult?) {
                    super.onLocationResult(p0)
                    presenter.location = p0?.lastLocation
                }
            }
            fusedLocationManager.requestLocationUpdates(LocationRequest(), locationCallback, null)
        }
        else requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION), 1)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 1){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                initLocationManager()
        }
    }

    override fun onDetach() {
        super.onDetach()
        fusedLocationManager.removeLocationUpdates(locationCallback)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.unbindView()
    }
}