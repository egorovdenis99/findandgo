package com.example.findandgo.ui

import android.content.Context
import android.os.Bundle
import android.support.annotation.ColorRes
import android.support.v4.app.FragmentTransaction
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter
import com.example.findandgo.R
import kotlinx.android.synthetic.main.activity_main.*
import vds.example.myapplication.models.Event

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AHBottomNavigationAdapter(this, R.menu.bottom_nav_menu).apply {
            setupWithBottomNavigation(bottomBar)
        }

        with(bottomBar) {
            accentColor = context.color(android.R.color.white)
            inactiveColor = context.color(R.color.tab_color_inactive)
            titleState = AHBottomNavigation.TitleState.ALWAYS_SHOW
            defaultBackgroundColor = context.color(R.color.colorPrimary)
            currentItem = 1
            setOnTabSelectedListener { position, wasSelected ->
                when(position){
                    0 -> {
                        val fragment = supportFragmentManager.findFragmentByTag(MAP_FRAGMENT_TAG) as? EventsMapFragment
                        if (fragment != null){
                            supportFragmentManager.beginTransaction()
                                .addToBackStack(MAP_FRAGMENT_TAG)
                                .replace(R.id.content, fragment)
                                .commit()
                        }
                        else {
                            supportFragmentManager.beginTransaction()
                                .addToBackStack(MAP_FRAGMENT_TAG)
                                .replace(R.id.content, EventsMapFragment())
                                .commit()
                        }
                        true
                    }
                    1 -> {
                        val fragment = supportFragmentManager.findFragmentByTag(EVENTS_FRAGMENT_TAG) as? EventsFragment
                          fragment?.messageListener =   object : MessageListener<Event>{
                              override fun onMessage(message: Event) {
                                  supportFragmentManager.beginTransaction()
                                      .addToBackStack(DETAILS_FRAGMENT_TAG)
                                      .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                      .replace(R.id.content, DetailFragment.newInstance(message))
                                      .commit()
                              }

                          }

                        if (fragment != null){
                            supportFragmentManager.beginTransaction()
                                .addToBackStack(EVENTS_FRAGMENT_TAG)
                                .replace(R.id.content, fragment)
                                .commit()
                        }
                        else {
                            supportFragmentManager.beginTransaction()
                                .addToBackStack(EVENTS_FRAGMENT_TAG)
                                .replace(R.id.content, EventsFragment().apply {
                                    messageListener =  object : MessageListener<Event>{
                                        override fun onMessage(message: Event) {
                                            supportFragmentManager.beginTransaction()
                                                .addToBackStack(DETAILS_FRAGMENT_TAG)
                                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                                                .replace(R.id.content, DetailFragment.newInstance(message))
                                                .commit()
                                        }

                                    }
                                })
                                .commit()
                        }
                        true
                    }
                    2 -> {
                        val fragment = supportFragmentManager.findFragmentByTag(MAP_FRAGMENT_TAG) as? EventsMapFragment
                        if (fragment != null){
                            supportFragmentManager.beginTransaction()
                                .addToBackStack(MAP_FRAGMENT_TAG)
                                .replace(R.id.content, fragment)
                                .commit()
                        }
                        else {
                            supportFragmentManager.beginTransaction()
                                .addToBackStack(MAP_FRAGMENT_TAG)
                                .replace(R.id.content, EventsMapFragment())
                                .commit()
                        }
                        true
                    }
                    else -> false
                }
            }
        }


        if (savedInstanceState == null){
            val fragment = EventsFragment().apply {
                messageListener = object : MessageListener<Event>{
                    override fun onMessage(message: Event) {
                        supportFragmentManager.beginTransaction()
                            .addToBackStack(DETAILS_FRAGMENT_TAG)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .replace(R.id.content, DetailFragment.newInstance(message))
                            .commit()
                    }

                }
            }

            supportFragmentManager.beginTransaction()
                .addToBackStack(EVENTS_FRAGMENT_TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(R.id.content, fragment)
                .commit()
        }

    }

    companion object{
        const val EVENTS_FRAGMENT_TAG = "events_fragment_tag"
        const val DETAILS_FRAGMENT_TAG = "details_fragment_tag"
        const val MAP_FRAGMENT_TAG = "map_fragment_tag"
    }

    private fun Context.color(@ColorRes colorRes: Int) = ContextCompat.getColor(this, colorRes)

}
