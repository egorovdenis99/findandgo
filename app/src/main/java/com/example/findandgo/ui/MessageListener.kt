package com.example.findandgo.ui

interface MessageListener<T> {
    fun onMessage(message: T)
}