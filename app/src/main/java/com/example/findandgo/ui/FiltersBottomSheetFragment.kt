package com.example.findandgo.ui

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.TextView
import com.example.findandgo.R
import kotlinx.android.synthetic.main.fragment_filter.*
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.lang.Integer.parseInt
import java.lang.Long.parseLong
import java.util.*

class FiltersBottomSheetFragment: BottomSheetDialogFragment() {

    var messageListener: MessageListener<FilterState>? = null

    val filter: FilterState = FilterState.EMPTY

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_filter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        doneButton.setOnClickListener {
            messageListener?.onMessage(
                filter.apply {
                    isFree = filterIsFreeSwitch.isChecked
                    radius =  filterRadiusEditText.text.toString().takeIf { it.isNotEmpty() }?.let { parseInt(it) }
                    city = (filterCity.adapter.getItem(filterCity.selectedItemPosition) as String)
                }
            )
        }

        filterCity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                return
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                filter.city = when(filterCity.adapter.getItem(position)) {
                    0 -> "spb"
                    1 -> "msk"
                    2 -> "ekb"
                    else -> "kzn"
                }
            }
        }

        filterDateFrom.setOnClickListener { showDatePicker(requireContext(), filterDateFrom)}
        filterDateTo.setOnClickListener { showDatePicker(requireContext(), filterDateTo)}

    }

    override fun onDetach() {
        super.onDetach()
        messageListener = null
    }

    private fun showDatePicker(context: Context, textView: View) {
        val c = Calendar.getInstance()

        val datePicker = DatePickerDialog(context,
            DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                val time = Calendar.getInstance().also { it.set(year, month, dayOfMonth) }.timeInMillis
                val df = DateTimeFormat.forPattern("d MMMM")
                (textView as TextView).text = df.print(time)

                if (textView == filterDateFrom) {
                    filter.dateStart = time
                }
                if (textView == filterDateTo) {
                    filter.dateEnd = time
                }

            },
            c.get(Calendar.YEAR),
            c.get(Calendar.MONTH),
            c.get(Calendar.DAY_OF_MONTH))
            .show()
    }



}