package com.example.findandgo.ui

class FilterState(
    var isFree: Boolean = true,
    var dateStart: Long? = null,
    var dateEnd: Long? = null,
    var radius: Int? = null,
    var city: String = "spb"
) {

    companion object {
        val EMPTY = FilterState()
    }

}