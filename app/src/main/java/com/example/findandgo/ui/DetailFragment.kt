package com.example.findandgo.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.findandgo.R
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.android.synthetic.main.item_event.view.*
import org.joda.time.format.DateTimeFormat
import vds.example.myapplication.models.Event

class DetailFragment: Fragment() {

    private lateinit var event: Event


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        retainInstance = true
        arguments?.let {
            event = it[EXTRA_EVENT] as Event
        }
        title.text = event.title
        val start = event.dates.first().dateStart
        val end = event.dates.first().dateEnd

        val df = DateTimeFormat.forPattern("d MMMM, hh:mm")
        date.text = "${df.print(start)} - ${df.print(end)}"

        description.text = event.description
        body_text.text = event.bodyText
        price.text = event.price

        Glide.with(this)
            .load(event.images.first().imageUrl)
            .into(imageView)

    }


    companion object{
        const val EXTRA_EVENT = "key_event"
        fun newInstance(event: Event) = DetailFragment().apply {
            arguments = Bundle().apply {
                putSerializable(EXTRA_EVENT, event)
            }
        }
    }
}