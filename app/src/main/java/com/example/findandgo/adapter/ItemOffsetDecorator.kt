package com.example.findandgo.adapter

import android.content.Context
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class ItemOffsetDecorator(val top: Int, val bottom: Int, val start: Int, val end: Int): RecyclerView.ItemDecoration(){
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.set(start.dpToPx(parent.context), top.dpToPx(parent.context), end.dpToPx(parent.context), bottom.dpToPx(parent.context))
    }
}

fun Int.dpToPx(context: Context): Int = (this * context.resources.displayMetrics.density).toInt()