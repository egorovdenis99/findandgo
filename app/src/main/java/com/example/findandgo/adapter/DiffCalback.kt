package com.example.findandgo.adapter

import android.support.v7.util.DiffUtil
import vds.example.myapplication.models.Event

class DiffCalback(private val newList: List<Event>, private val oldList: List<Event>): DiffUtil.Callback() {

    override fun areItemsTheSame(p0: Int, p1: Int) = newList[p0].id == oldList[p1].id

    override fun getOldListSize() = oldList.size

    override fun getNewListSize() = newList.size

    override fun areContentsTheSame(p0: Int, p1: Int) = newList[p0] == oldList[p1]


}