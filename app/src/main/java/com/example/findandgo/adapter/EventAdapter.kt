package com.example.findandgo.adapter

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.findandgo.R
import kotlinx.android.synthetic.main.item_event.view.*
import org.joda.time.format.DateTimeFormat
import vds.example.myapplication.models.Event
import java.util.*


class EventAdapter: RecyclerView.Adapter<EventAdapter.EventViewHolder>() {

    var events = listOf<Event>()

    interface OnEventClickListener{
        fun onEventClick(event: Event)
    }

    var onEventClickListener: OnEventClickListener? = null

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): EventViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_event, viewGroup, false)
        return EventViewHolder(view)
    }

    override fun getItemCount() = events.size

    override fun onBindViewHolder(viewHolder: EventViewHolder, position: Int) {
        viewHolder.bind(events[position])
    }

    fun updateEvents(newEvents: List<Event>){
        val diffCallback = DiffCalback(newEvents, events)
        events = newEvents
        DiffUtil.calculateDiff(diffCallback).dispatchUpdatesTo(this)
    }

    inner class EventViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun bind(event: Event){
            with(itemView){
                Glide.with(this)
                    .load(event.images.first().imageUrl)
                    .into(eventImage)

                eventTitle.text = event.title

                val start = event.dates.first().dateStart
                val end = event.dates.first().dateEnd

                val df = DateTimeFormat.forPattern("d MMMM, hh:mm")
                eventLocation.text = "${df.print(start)} - ${df.print(end)}"

                setOnClickListener { onEventClickListener?.onEventClick(event) }

            }
        }
    }
}