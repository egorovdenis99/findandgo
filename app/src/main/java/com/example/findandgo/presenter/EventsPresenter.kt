package com.example.findandgo.presenter

import android.location.Location
import com.example.findandgo.repository.EventRepository
import com.example.findandgo.ui.FilterState
import com.example.findandgo.view.MvpView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import vds.example.myapplication.models.EventResponse

class EventsPresenter(private val repository: EventRepository): BasePresenter<MvpView>(){
    var location: Location? = null
    private var currentState: EventState? = null
    fun onFiltersSelected(filter: FilterState){
        currentState = EventState(true, false, null)
        view?.render(currentState!!)
        repository.loadEvents(filter)
            .enqueue(object : Callback<EventResponse>{
                override fun onFailure(call: Call<EventResponse>, t: Throwable) {
                    currentState = EventState(false, true, null)
                    view?.render(currentState!!)
                }

                override fun onResponse(call: Call<EventResponse>, response: Response<EventResponse>) {
                    currentState = EventState(false, false, response.body()!!.results)
                    view?.render(currentState!!)
                }

            })
    }

    fun restoreState(){
        if (currentState != null)
            view?.render(currentState!!)
    }
}