package com.example.findandgo.presenter

import com.example.findandgo.repository.EventRepository
import com.example.findandgo.view.MvpView

class DetailtPresenter(private val repository: EventRepository): BasePresenter<MvpView>(){

    fun onCreate(postId: Int){
        repository.loadLocationInfo(postId)
    }

}