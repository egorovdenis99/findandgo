package com.example.findandgo.presenter

import vds.example.myapplication.models.Event

data class EventState(
    var isLoading: Boolean = false,
    var isError: Boolean = false,
    var events: List<Event>? = null
)