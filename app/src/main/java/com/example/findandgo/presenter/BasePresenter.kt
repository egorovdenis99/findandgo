package com.example.findandgo.presenter

import com.example.findandgo.view.MvpView

abstract class BasePresenter<V : MvpView> {
    var view: V? = null

    fun bindView(view: V) {
        this.view = view
    }

    fun unbindView() {
        this.view = null
    }
}