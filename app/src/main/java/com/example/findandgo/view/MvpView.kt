package com.example.findandgo.view

import com.example.findandgo.presenter.EventState

interface MvpView {
    fun render(state: EventState)
}