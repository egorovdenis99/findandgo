package com.example.findandgo

import com.example.findandgo.network.EventsService
import com.example.findandgo.presenter.DetailtPresenter
import com.example.findandgo.presenter.EventsPresenter
import com.example.findandgo.repository.EventRepository
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object DependencyModule {

    private var retrofit: Retrofit? = null
    private var eventService: EventsService? = null
    var eventRepository: EventRepository? = null
    private var eventPresenter: EventsPresenter? = null
    private var detailtPresenter: DetailtPresenter? = null

    fun start(){
        retrofit = getRetrofit()
        eventService = getEventsService()
        eventRepository = getEventsRepository(eventService!!)
        eventPresenter = getEventPresenter(eventRepository!!)
        detailtPresenter = getDetailPresenter(eventRepository!!)
    }

    private fun getRetrofit() = Retrofit.Builder()
        .baseUrl(getBaseUrl())
        .addConverterFactory(getConverterFactory())
        .build()


    private fun getConverterFactory() = GsonConverterFactory.create()

    private fun getBaseUrl() = BuildConfig.SERVER_ADDRESS

    private fun getEventsService() = retrofit?.create(EventsService::class.java)

    private fun getEventsRepository(service: EventsService) = EventRepository(service)

    private fun getEventPresenter(repository: EventRepository) = EventsPresenter(repository)

    private fun getDetailPresenter(repository: EventRepository) = DetailtPresenter(repository)

    fun providePresenter() = eventPresenter as EventsPresenter

    fun provideDetailPresenter() = detailtPresenter as DetailtPresenter
}