package vds.example.myapplication.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class EventResponse(
     @SerializedName("count") val count: Int,
     @SerializedName("next") val next: String,
     @SerializedName("previous") val previous: String,
     @SerializedName("results") val results: List<Event>
)

data class Event(

    @SerializedName("id")
    val id: Long,

    @SerializedName("publication_date")
    val publishDate: Long,

    @SerializedName("dates")
    val dates: List<DatePeriod>,

    @SerializedName("title")
    val title: String,

    @SerializedName("slug")
    val slug: String,

    @SerializedName("place")
    val place: Place,

    @SerializedName("description")
    val description: String,

    @SerializedName("body_text")
    val bodyText: String,

    @SerializedName("location")
    val location: Location,

    @SerializedName("categories")
    val categories: List<String>,

    @SerializedName("price")
    val price: String,

    @SerializedName("is_free")
    val isFree: Boolean,

    @SerializedName("images")
    val images: List<Image>,

    @SerializedName("short_title")
    val shortTitle: String

) : Serializable

data class DatePeriod(

    @SerializedName("start")
    val dateStart: Long,

    @SerializedName("end")
    val dateEnd: Long

)

data class Image(

    @SerializedName("image")
    val imageUrl: String,

    @SerializedName("source")
    val source: Source
)

data class Source(

    @SerializedName("name")
    val name: String,

    @SerializedName("link")
    val link: String
)

data class Location(

    @SerializedName("slug")
    val slug: String
)

data class Place(
    @SerializedName("id")
    val id: Long
)