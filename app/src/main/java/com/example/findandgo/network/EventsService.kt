package com.example.findandgo.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap
import vds.example.myapplication.models.EventResponse

interface EventsService{

    @GET("public-api/v1.4/events/")
    fun loadEvents(@QueryMap eventsRequest: HashMap<String, String>): Call<EventResponse>

    @GET("public-api/v1.4/places/{place_id}/?fields=address,location")
    fun loadLocationInfo(@Path("place_id") id: Int)
}